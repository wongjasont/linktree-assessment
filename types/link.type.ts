type Link = {
  id?: string
  title: string
  type: LinkType
  userId: string
  url?: string
  // @todo: idea is to have a freeform field that can contain all the custom fields for special link types
  // This is in a sense future proof because any new type can be introduced, and any form of data
  // can be used for the value, the common fields are the only ones defined
  data?: []
}

enum LinkType {
  Classic = 'classic',
  Show = 'show',
  Music = 'music',
}

export { Link, LinkType }
