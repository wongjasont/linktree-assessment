import express, { Request, Response } from 'express'
import { object, string } from 'yup'
import { clearLinks, createLink, deleteLink, getLink, listLinks, updateLink } from '../service'
import { LinkType } from '../types'
import { catchError, validateBody } from '../util'

const router = express.Router()

router.get(
  '/',
  catchError(async (req: Request, res: Response) => {
    const result = await listLinks()
    res.json(result)
  })
)

router.get(
  '/:id',
  catchError(async (req: Request, res: Response) => {
    const id = req.params.id
    const result = await getLink(id)
    res.json(result)
  })
)

let linkSchema = object().shape({
  title: string().max(144),
  // @todo: URL Validation is just a sample, but use regex to validate, and maybe only allow HTTPS
  url: string().matches(
    /((https?):\/\/)?(www.)?[a-z0-9]+(\.[a-z]{2,}){1,3}(#?\/?[a-zA-Z0-9#]+)*\/?(\?[a-zA-Z0-9-_]+=[a-zA-Z0-9-%]+&?)?$/,
    'Enter correct url!'
  ),
  type: string().oneOf(Object.values(LinkType)),
  userId: string(),
})

router.post(
  '/',
  validateBody(linkSchema),
  catchError(async (req: Request, res: Response) => {
    const object = req.body as any
    const result = await createLink(object)
    res.json(result)
  })
)

router.put(
  '/:id',
  catchError(async (req: Request, res: Response) => {
    const id = req.params.id
    const object = req.body
    const result = await updateLink(id, object)
    res.json(result)
  })
)

router.delete(
  '/:id',
  catchError(async (req: Request, res: Response) => {
    const id = req.params.id
    const result = await deleteLink(id)
    res.json(result)
  })
)

router.delete(
  '/',
  catchError(async (req: Request, res: Response) => {
    const result = await clearLinks()
    res.json(result)
  })
)

export { router }
