import express from 'express'
import { router as linksRouter } from './links.router'
import { router as healthRouter } from './health'

const router = express.Router()

router.use('/links', linksRouter)
router.use(healthRouter)

export { router }
