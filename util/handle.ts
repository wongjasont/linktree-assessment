/*************************************************************
 * This is a copy-paste from a past project :-)
 *
 * Middle ware to catch errors so that it will not break our server
 * ************************************************************/
import { Request, Response, NextFunction } from 'express'

const catchError = (fn: Function) => {
  const isAsync = fn.constructor.name === 'AsyncFunction'
  if (isAsync) {
    return (req: Request, res: Response, next: NextFunction) => {
      fn(req, res, next)
        .then((_: any) => {}, next) // Rejections will be treated like errors
        .catch(next) // Catch thrown errors
    }
  } else {
    return (req: Request, res: Response, next: NextFunction) => {
      try {
        fn(req, res, next)
      } catch (err) {
        next(err)
      }
    }
  }
}

export { catchError }
