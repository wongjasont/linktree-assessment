/*********************************************************
 * This file is a copy-paste from a past project :-)
 *
 * This allows to pair a yupSchema, then use it as a middle ware to assert schema input
 * ********************************************************/
import {ObjectSchema, ValidationError} from 'yup'
import {Request, Response, NextFunction} from 'express'

const validate = (
  yupSchema: ObjectSchema<any>,
  { from = 'body', status = 400, returnErrors = true } = {}
) => {
  return async (req: Request, res: Response, next: NextFunction) => {

    try {
      // @ts-ignore
      await yupSchema.validate(req[from], { strict: true })
      // @ts-ignore
      req[from] = yupSchema.cast(req[from])
      next()
      // @ts-ignore
    } catch (err: ValidationError) {
      if (returnErrors) {
        return res.status(status).json({ errors: err.errors })
      } else {
        return res.status(status).send()
      }
    }
  }
}

const validateBody = (yupSchema: ObjectSchema<any>, { status = 400, returnErrors = true } = {}) => {
  return validate(yupSchema.noUnknown(true), {
    status,
    returnErrors,
    from: 'body',
  })
}

const validateQuery = (yupSchema: ObjectSchema<any>, { status = 400,  returnErrors = true } = {}) => {
  return validate(yupSchema, { status, returnErrors, from: 'query' })
}

const validateParams = (yupSchema: ObjectSchema<any>, { status = 400,  returnErrors = true } = {}) => {
  return validate(yupSchema, { status, returnErrors, from: 'params' })
}

export {
  validateParams,
  validateBody,
  validateQuery,
}
