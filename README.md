<p align="center">
  <img src="https://github.com/blstrco/linktr.ee-backend-assessment/raw/master/Screen%20Shot%202019-07-08%20at%202.09.47%20pm.png">
</p>

# The Problem
We have three new link types for our users.

1. Classic
	- Titles can be no longer than 144 characters.
	- Some URLs will contain query parameters, some will not.
2. Shows List
	- One show will be sold out.
	- One show is not yet on sale.
	- The rest of the shows are on sale.
3. Music Player
	- Clients will need to link off to each individual platform.
	- Clients will embed audio players from each individual platform.

You are required to create a JSON API that our front end clients will interact with.

- The API can be GraphQL or REST.
- The API can be written in your preferred language.
- The client must be able to create a new link of each type.
- The client must be able to find all links matching a particular userId.
- The client must be able to find links matching a particular userId, sorted by dateCreated.


## Your Solution

- Consider bad input data and the end user of your API - we're looking for good error handling and input validation.
- If you are creating a GraphQL API, think about the access patterns the client may use, and think about the acces patterns the client may not use. Try not to [Yak Shave](https://seths.blog/2005/03/dont_shave_that/)
- Consider extensibility, these are 3 of hundreds of potential link types that we will be developing.


## Rules & Tips

- Choose the language and environment of your choice, just include documentation on how to run your code.
- Immutability and functional programming is looked upon favorably.
- You cannot connect to a real world database - document your schema design.
- Mocking third parties is looked upon favorably.
- @todo comments are encouraged. You aren't expected to complete the challenge, but how you design your solution and your ideas for the future are important.

---
# Submission
Set up your own remote git repository and make commits as you would in your day to day work. Submit a link to your repo when you're finished.

---

# Notes to Assessor:

This was a fun assessment. I spent about 30 mins understanding the instruction first before jumping into the coding and putting it in gitlab.

I wanted to understand the best way to write this, I initially thought JS would be good, but I ended up using typescript because I found a tutorial
that helped me set up Express from ground up (usually I use the express cli generator). Also, I already have a lot of existing express projects
lying around so much of the work is putting new code together plus some reused code, and making it work.

Definitely left a number of todos -- I think I might have used jest mocks instead of actually implementing the dummy links service file.
I honestly panicked in doing this and ended up "mocking" (not using jest mocks but in memory implementation) for the links service to simulate
a DB service.

Another one is the validation, I believe there are a number things I left out and I was simply just out of time. But the gist is the same, I've used
"yup" before so I actually reused some of my existing scripts, and I think in practice it is usually like that. Other future link types would just
have a separate yup object schema, then use the validate function to force to throw an error if the validation fails, then just hook it to
the error handler (thus the Error code 400 when that particularly happens) -- see util/validate.ts

Another one is the query filter function which I totally forgot to do. That one was easy, it just require some wiring against the query param,
use keys to filter the objects. Implementations like this usually relies on the data store or your search engine.

I would have fixed the directories more. Usually we'd need to put everything "code" into src folder, then have build for build files
and build process, etc, but I think I missed doing it here in a rush.

For tests, I would usually create api tests as well -- these test the error codes and bad request errors that validation would catch. But I skipped that.

I believe I spent most of my time actually setting up the Express App and not focused on solutionizing the link types. I believe there are more
things I would do here particularly identifying interfaces for Links instead of mere types, More Link types would mean more metadata,
and there are just a lot of services that must have a way to distinguish these types aside from the type and allow pulling these data
(ex music would have the capability to connect to S3 bucket and to stream a file, API should support that), the shows type would have even more
metadata for cover (image) and other urls (that are not covered by the original url field). I believe the whole solutionizing part of the
Links require much BA work.

Lastly, all my commits are directly to master -- I don't do this at work, we work on feature-branches then merge it into master
(or develop first then master as in git-flow). It really depends on the dev team practices.

Made many mistakes in this test, but I'm glad I did because I reviewed express again after not using it for a couple of years.
Best teachers are our mistakes :-)

## Screenshots

![Error Handling](./screenshots/api-error-handling.png)
![Happy Path](./screenshots/happy-path.png)
![Unit Tests](./screenshots/unit-tests.png)

