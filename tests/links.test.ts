import { clearLinks, createLink, listLinks, deleteLink, updateLink, getLink } from '../service'
import { LinkType } from '../types'

beforeEach(async () => {
  await clearLinks()
})

test('listLinks', async () => {
  const data1 = await listLinks()
  expect(data1).toEqual(expect.arrayContaining([]))
  const created = await createLink({
    title: 'Test Title',
    type: LinkType.Classic,
    userId: '1',
    url: 'https://google.com',
  })
  const data2 = await listLinks()
  expect(data2).toMatchObject([created])
})

test('createLink', async () => {
  const created = await createLink({
    title: 'Test Title',
    type: LinkType.Classic,
    userId: '1',
    url: 'https://google.com',
  })
  expect(created.title).toEqual('Test Title')
  expect(created.type).toEqual('classic')
  expect(created.userId).toEqual('1')
  expect(created.url).toEqual('https://google.com')
})

test('deleteLink', async () => {
  const created = await createLink({
    title: 'Test Title',
    type: LinkType.Classic,
    userId: '1',
    url: 'https://google.com',
  })
  const data1 = await listLinks()
  expect(data1.length).toEqual(1)
  const deleted = await deleteLink(created.id!)
  const data2 = await listLinks()
  expect(data2.length).toEqual(0)
})

test('updateLink', async () => {
  const created = await createLink({
    title: 'Test Title',
    type: LinkType.Classic,
    userId: '1',
    url: 'https://google.com',
  })
  const data1 = await listLinks()
  expect(data1.length).toEqual(1)
  const updated = await updateLink(created.id!, { title: 'New Title' })
  expect(updated!.title).toEqual('New Title')
})

test('getLink', async () => {
  const created1 = await createLink({
    title: 'Test Title 1',
    type: LinkType.Classic,
    userId: '1',
    url: 'https://google.com',
  })
  const created2 = await createLink({
    title: 'Test Title 2',
    type: LinkType.Music,
    userId: '1',
    url: 'https://yahoo.com',
  })
  const list = await listLinks()
  expect(list.length).toEqual(2)
  const fetched = await getLink(created1.id!)
  expect(fetched!.title).toEqual('Test Title 1')
})
