import express from 'express'
import { json } from 'body-parser'
import { router } from './router'
// rest of the code remains same
const app = express()
const PORT = 8000

app.use(json())
app.use(router)

app.listen(PORT, () => {
  console.log(`⚡️[server]: Server is running at https://localhost:${PORT}`)
})
