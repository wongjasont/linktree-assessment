import { Link } from '../types'
import { randomUUID } from 'crypto'

const linksData: { [key: string]: Link } = {}

const createLink = async (link: Link): Promise<Link> => {
  const object = {
    ...link,
    id: randomUUID(), // mock creation of id
  }
  linksData[object.id] = object
  return object
}

const deleteLink = async (id: string): Promise<Link | null> => {
  if (linksData[id]) {
    const object = linksData[id]
    delete linksData[id]
    return object
  }
  return null
}

const getLink = async (id: string): Promise<Link | null> => {
  if (linksData[id]) {
    return linksData[id]
  }
  return null
}

const updateLink = async (id: string, linkUpdate: any): Promise<Link | null> => {
  if (linksData[id]) {
    linksData[id] = { ...linksData[id], ...linkUpdate }
    return linksData[id]
  }
  return null
}

const listLinks = async (): Promise<Link[]> => {
  return Object.values(linksData)
}

const clearLinks = async (): Promise<Link[]> => {
  const links = Object.values(linksData)
  Object.keys(linksData).forEach((k) => delete linksData[k])
  return links
}

export { createLink, getLink, listLinks, deleteLink, updateLink, clearLinks }
